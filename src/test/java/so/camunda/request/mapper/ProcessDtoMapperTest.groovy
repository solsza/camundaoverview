package so.camunda.request.mapper

import org.camunda.bpm.engine.repository.ProcessDefinition
import so.camunda.request.dto.ProcessDto
import spock.lang.Specification

class ProcessDtoMapperTest extends Specification {

    static ProcessDefinition processDefinition;
    static ProcessDefinition processDefinition1;
    static ProcessDto processDto;


    def setupSpec() {
        processDefinition = Mock(ProcessDefinition.class);
        processDefinition1 = Mock(ProcessDefinition.class);
        processDto = new ProcessDto("testId", "testProcessName", "testProcessVersion")
    }

    def "should return ProcessDto from Process Definition"() {
        given:
        processDefinition.id >> "1"
        processDefinition.name >> "processDefinitionName"
        processDefinition.version >> Integer.valueOf("1")
        when:
        def dto = ProcessDtoMapper.mapToDto(processDefinition)
        then:
        dto.getProcessId() == processDefinition.getId()
        dto.getProcessName() == processDefinition.getName()
        dto.getProcessVersion() == String.valueOf(processDefinition.getVersion())
    }

    def "should return List<ProcessDto> from List<ProcessDefinition>" () {
        given:
        processDefinition.id >> "1"
        processDefinition.name >> "processDefinitionName"
        processDefinition.version >> Integer.valueOf("1")
        processDefinition1.id >> "2"
        processDefinition1.name >> "processDefinitionName1"
        processDefinition1.version >> Integer.valueOf("2")
        List<ProcessDefinition> processDefinitionList = new LinkedList<>()
        processDefinitionList.add(processDefinition1)
        processDefinitionList.add(processDefinition1)
        when:
        def processDtoList = ProcessDtoMapper.mapToDtos(processDefinitionList)
        then:
        processDtoList.size() == 2
        processDtoList.get(0).getProcessId() == processDefinition.getId()
        processDtoList.get(0).getProcessName() == processDefinition.getName()
        processDtoList.get(0).getProcessVersion() == String.valueOf(processDefinition.getVersion())
        processDtoList.get(1).getProcessId() == processDefinition1.getId()
        processDtoList.get(1).getProcessName() == processDefinition1.getName()
        processDtoList.get(1).getProcessVersion() == String.valueOf(processDefinition1.getVersion())
    }

}
