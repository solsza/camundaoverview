package so.camunda.request.mapper

import org.camunda.bpm.engine.runtime.ProcessInstance
import so.camunda.request.dto.ProcessDto
import spock.lang.Specification

class ProcessInstanceDtoMapperTest extends Specification {

    static ProcessInstance processInstance
    static ProcessInstance processInstance1
    static ProcessDto processDto

    def setupSpec() {
        processDto = new ProcessDto("processTestID", "processTestName", "processTestVersion");
        processInstance = Mock(ProcessInstance.class)
        processInstance1 = Mock(ProcessInstance.class)
    }

    def "should return ProcessInstanceDto from ProcessInstance"() {
        given:
        processInstance.id >> "testID"
        processInstance.getProcessDefinitionId() >> "processDefinitionId"
        when:
        def dto = ProcessInstanceDtoMapper.mapToDto(processInstance);
        then:
        dto.getInstanceId() == processInstance.getId()
        dto.getInstanceName() == processInstance.getProcessDefinitionId()
    }

    def "should return List<ProcessInstanceDto> from List<ProcessInstance>" (){
        given:
        processInstance.id >> "testID"
        processInstance.getProcessDefinitionId() >> "processDefinitionId"
        processInstance1.id >> "testID1"
        processInstance1.getProcessDefinitionId() >> "processDefinitionId1"
        List<ProcessInstance> processInstanceList = new LinkedList<>()
        processInstanceList.add(processInstance)
        processInstanceList.add(processInstance1)
        when:
        def dtos = ProcessInstanceDtoMapper.mapToDtos(processInstanceList)
        then:
        dtos.size() == 2;
        dtos.get(0).getInstanceId() == processInstance.getId()
        dtos.get(0).getInstanceName() == processInstance.getProcessDefinitionId()
        dtos.get(1).getInstanceId() == processInstance1.getId()
        dtos.get(1).getInstanceName() == processInstance1.getProcessDefinitionId()
    }
}
