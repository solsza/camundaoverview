package so.camunda.request.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ProcessInstancesDto {

    private String instanceId;
    private String instanceName;
}
