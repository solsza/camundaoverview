package so.camunda.request.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class TaskDto {

    private String taskId;
    private String taskName;

}
