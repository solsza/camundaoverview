package so.camunda.request.taskDelagate;

import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

/*
Class for ValidRequest Task
Component annotation is needed for using Delegate Expression in Camunda Modeler
 */
@Component
@Log4j2
public class RequestValid implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        String status = (String) delegateExecution.getVariable("status");
        delegateExecution.setVariable("status", "valid");
        ///set variable valid use in bpmn after gateway
        delegateExecution.setVariable("validation", true);
        log.info("requestValid");
    }
}
