package so.camunda.request.taskDelagate;

import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;

import java.util.Random;

/*
Class for Request Task
Component annotation is needed for using Delegate Expression in Camunda Modeler
 */
@Component
@Log4j2
public class RequestDelegate implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        //return random boolean value to move forward
        Random random = new Random();
        //set variable valid use in bpmn after gateway
        delegateExecution.setVariable("valid", random.nextBoolean());
        log.info("requestDelegate");
    }
}
