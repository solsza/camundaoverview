package so.camunda.request.taskDelagate;

import lombok.extern.log4j.Log4j2;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.springframework.stereotype.Component;


/*
Class for CreateATARS Task
Component annotation is needed for using Delegate Expression in Camunda Modeler
 */
@Component
@Log4j2
public class CreateATARS implements JavaDelegate {

    @Override
    public void execute(DelegateExecution delegateExecution) throws Exception {

        delegateExecution.setVariable("status", "create");
        log.info("createATARS");
    }
}
