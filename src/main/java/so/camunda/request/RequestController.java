package so.camunda.request;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.task.Task;
import org.springframework.web.bind.annotation.*;
import so.camunda.request.dto.ProcessDto;
import so.camunda.request.dto.ProcessInstancesDto;
import so.camunda.request.dto.TaskDto;
import so.camunda.request.mapper.ProcessDtoMapper;
import so.camunda.request.mapper.ProcessInstanceDtoMapper;
import so.camunda.request.mapper.TaskDtoMapper;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/requests")
@RequiredArgsConstructor
public class RequestController {

    private final RequestService requestService;

    @PostMapping("/sendRequest")
    public boolean startTheProcess(@RequestBody Request request) {
        return requestService.startTheProcess(request);
    }

    // list of all active instance for process (processName)
    @GetMapping("/getActiveInstancesOfProcess/{processName}")
    public List<ProcessInstancesDto> getActiveInstancesOfProcesses(@PathVariable String processName) {
        return ProcessInstanceDtoMapper.mapToDtos(requestService.getAllRunningInstancesForProcess(processName));
    }

    // list of all processes latest version
    @GetMapping("/getProcesses")
    public List<ProcessDto> getProcesses() {
        return ProcessDtoMapper.mapToDtos(requestService.getProcesses());
    }


    // list of variables for instance of process
    @GetMapping("/getProcessInstanceVariables/{instanceId}")
    public Map<String, Object> getInstanceVariables(@PathVariable String instanceId) {
        return requestService.getVariablesForProcess(instanceId);
    }

    //set variables of process
    @PostMapping("/setVariablesOfProcessRuntime/{instanceId}")
    public Map<String, Object> setVariablesForProcessInstance (@PathVariable String instanceId, @RequestBody Map<String, Object> mapOfVariables){
        return requestService.setVariablesForProcessInstance(instanceId, mapOfVariables);
    }

    //list of tasks fot process instance id
    @GetMapping("/getTaskList/{instanceId}")
    public List<TaskDto> getTaskList (@PathVariable String instanceId){
        return TaskDtoMapper.mapToDtos(requestService.getTaskForProcessInstance(instanceId));
    }

    //accept user task
    @GetMapping("/acceptUserTask/{taskId}")
    public TaskDto acceptUserTask (@PathVariable String taskId){
        return TaskDtoMapper.mapToDto(requestService.acceptUserTask(taskId));
    }

    //accept user task with variables
    @PostMapping("/acceptUserTaskWithVariables/{taskId}")
    public TaskDto acceptUserTaskWithVariables (@PathVariable String taskId, @RequestBody Map<String, Object> mapOfVariables){
        return TaskDtoMapper.mapToDto(requestService.acceptUserTaskWithVariables(taskId, mapOfVariables));
    }

}
