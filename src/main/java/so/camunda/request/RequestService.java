package so.camunda.request;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.RepositoryService;
import org.camunda.bpm.engine.RuntimeService;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.engine.repository.ProcessDefinition;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import org.camunda.bpm.engine.task.Task;
import org.camunda.bpm.engine.variable.Variables;
import org.springframework.stereotype.Service;
import so.camunda.request.dto.TaskDto;
import so.camunda.request.property.ProcessConstants;

import java.util.List;
import java.util.Map;

/*
Example of start new process based on bpmn file existing in class path, id of bpmn must by the same like Proccess_key
 */
@Service
@RequiredArgsConstructor
public class RequestService {

    private final ProcessEngine processEngine;
    private final RuntimeService runtimeService;
    private final RepositoryService repositoryService;

    public boolean startTheProcess(Request request) {

        //Start the example process parameters request id and status
        ProcessInstance processInstance = runtimeService
                .startProcessInstanceByKey(ProcessConstants.PROCESS_KEY, Variables.putValue("id", request.getId()).putValue("status", request.getStatus()));

        return processInstance.isEnded();
    }

    public List<ProcessInstance> getAllRunningInstancesForProcess(String processName) {


        //get latest process definition for process (processName)
        ProcessDefinition processDefinition =
                repositoryService.createProcessDefinitionQuery()
                        .processDefinitionName(processName)
                        .latestVersion()
                        .singleResult();

        //list for all active instance of active process, latest version (processName)
        List<ProcessInstance> processInstances =
                runtimeService.createProcessInstanceQuery()
                        .processDefinitionId(processDefinition.getId())
                        .active()
                        .list();
        return processInstances;
    }

    public List<ProcessDefinition> getProcesses() {
        // get list of active process . latest version
        List<ProcessDefinition> processDefinition = repositoryService.createProcessDefinitionQuery().active().latestVersion().list();
        return processDefinition;
    }

    public Map<String, Object> getVariablesForProcess(String instanceId) {
        //get values for instance of the process
        Map<String, Object> values = runtimeService.getVariables(instanceId);
        return values;
    }

    public Map<String, Object> setVariablesForProcessInstance(String instanceId, Map<String, Object> mapOfVariables) {
        //set values for instance of the process
        runtimeService.setVariables(instanceId,mapOfVariables);
        return runtimeService.getVariables(instanceId);
    }

    public List<Task> getTaskForProcessInstance(String instanceId) {
        //get list of task for process key
        List<Task> tasks = processEngine.getTaskService().createTaskQuery().executionId(instanceId).list();
        return  tasks;
    }

    public Task acceptUserTask(String taskId) {
         Task task = processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
         processEngine.getTaskService().complete(taskId);
         return task;
    }

    public Task acceptUserTaskWithVariables(String taskId, Map<String, Object> mapOfVariables) {
        Task task = processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
        processEngine.getTaskService().complete(taskId, mapOfVariables);
        return task;
    }

}
