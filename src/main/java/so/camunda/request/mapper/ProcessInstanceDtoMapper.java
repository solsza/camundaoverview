package so.camunda.request.mapper;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.camunda.bpm.engine.runtime.ProcessInstance;
import so.camunda.request.dto.ProcessInstancesDto;

import java.util.List;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ProcessInstanceDtoMapper {

    public static List<ProcessInstancesDto> mapToDtos(List<ProcessInstance> processInstances) {
        return processInstances.stream()
                .map(processInstance -> mapToDto(processInstance))
                .collect(Collectors.toList());
    }

    public static ProcessInstancesDto mapToDto(ProcessInstance processInstance) {
        return ProcessInstancesDto.builder()
                .instanceId(processInstance.getId())
                .instanceName(processInstance.getProcessDefinitionId())
                .build();
    }
}
