package so.camunda.file;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/*
Controller for uploading  bpmn diagrams to upload-dir -> see properties
*/
@RestController
@RequestMapping("/BPMN")
@RequiredArgsConstructor
@Log4j2
public class FileController {

    private final FileStorageService fileStorageService;

    @PostMapping("/upload")
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        String fileName = fileStorageService.storeFile(file);

        //Uri for downland uploaded file
        String fileDownlandUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/getBPMN/")
                .path(fileName)
                .toUriString();

        return new UploadFileResponse(fileName, fileDownlandUri, file.getContentType(), file.getSize());
    }
}
