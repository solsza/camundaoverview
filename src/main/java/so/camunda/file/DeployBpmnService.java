package so.camunda.file;

import lombok.RequiredArgsConstructor;
import org.camunda.bpm.engine.ProcessEngine;
import org.camunda.bpm.engine.repository.Deployment;
import org.camunda.bpm.model.bpmn.impl.BpmnParser;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDateTime;

@RequiredArgsConstructor
@Service
public class DeployBpmnService {

    private final ProcessEngine camunda;

    public String deployBpmn(String fileName, InputStream inputStream) {

        String deploymentName = "New Deployment " + LocalDateTime.now();

        Deployment deployment = camunda.getRepositoryService()
                .createDeployment()
                .name(deploymentName)
                .addInputStream(fileName, inputStream)
                .deploy();
        
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        BpmnParser bpmnParser = new BpmnParser();

        return deployment.getId();
    }
}