# CamundaOverview

Camunda Workflow Engine Overview:
1. Deploying BPMN file and create Process
2. Controller of process (accept Task, get process instances)
    1. Start process instance, getActive instance
    2. Get and set process Variables
    3. Get tasks for process instance 
    4. Accept user Task with and without Variables 
3. Example Workflow (request)

Resources:
1. Camunda Initializer : https://start.camunda.com/
2. Camunda Modeler Desktop Version (create, update BPMN) : https://camunda.com/download/modeler/
3. BPMN Modeler Web Application : https://github.com/bpmn-io/bpmn-js-examples/tree/master/properties-panel
4. External Task Client (java): https://github.com/camunda/camunda-external-task-client-java
5. External Task Client JS : https://github.com/camunda/camunda-external-task-client-js
6. Camunda Overview Tutorial :https://www.youtube.com/watch?v=l-sCUKQZ44s&list=PLJG25HlmvsOUnCziyJBWzcNh7RM5quTmv
7. Camunda Docs : https://docs.camunda.org/manual/7.14/
8. Camunda Best Practices : https://camunda.com/best-practices/